package aplicacion;

//import dominio.*;
import java.util.Scanner;
//import java.util.NoSuchElementException;
//import java.util.Random;
//import java.lang.IllegalStateException;
//import java.lang.ArithmeticException;
import java.util.InputMismatchException;

public class Principal{
	public static void main(String args[]){
		Scanner sc = new Scanner(System.in);
		String nombre;
		System.out.println("ESTO ES UN JUEGO EN VERSION ALFA, PORFAVOR TENGA EN CUENTA QUE HAY ERRORES (MUY POCOS EXTRAS O NINGUNO)");
		System.out.println("¿Cual es tu nombre de usuario?:");
		nombre = sc.next();
		int numero;
		System.out.println(nombre + " ponga un numero máximo para el juego que sea mayor que 1:");
		numero = sc.nextInt();
		int i = 0;
		try{
			while(numero < 2){
				System.out.println("Porfavor introduzca un numero mayor que 1 para jugar");
				numero = sc.nextInt();
			}
			int numeroRandom = (int) ((Math.random() * numero)+1);

			System.out.print("Escribe un numero para averiguar el numero: ");
			int nuevoNumero = sc.nextInt();
			/*while (nuevoNumero < 1){
			  System.out.println("Porfavor introduzca un numero que sea entre el 0 y el " + (numero+1));
			  nuevoNumero = sc.nextInt();
			  }*/

			int intentos = 0;
			while(i<1){
				if (numeroRandom == nuevoNumero) {  
					System.out.println("¡" + nombre + " has acertado!");
					intentos++;
					System.out.println("Has acertado en " + intentos + " intentos.");
					i++;

				}

				else if (numeroRandom > nuevoNumero){ 
					System.out.println("El número secreto es MAYOR que " + nuevoNumero);
					System.out.println(nombre + " introduce otro numero:");
					nuevoNumero = sc.nextInt();
					intentos++;
				}
				else if (numeroRandom < nuevoNumero){
					System.out.println("El número secreto es MENOR que " + nuevoNumero);
					System.out.println(nombre + " introduce otro numero:");
					nuevoNumero = sc.nextInt();
					intentos++;
				}
			}
		}catch(InputMismatchException e){
			System.out.println("¡Has introducido una letra no un numero!");
			System.out.println("Vuelva a introducir un numero:");
			numero = sc.nextInt();
		}
		sc.close();
	}
}
